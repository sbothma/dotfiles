PS1='\W: '
# source /Users/stephan/.oh-my-git/prompt.sh
source "${HOME}/.shell_prompt.sh"
eval "$(rbenv init -)"

RED='\033[0;31m'
NOCOL='\033[0m'

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

source ~/.bash_aliases
source ~/.bash_paths

# fortune | cowsay -e 'Oo' | lolcat -F 0.1

# The next line updates PATH for the Google Cloud SDK.
test -e "${HOME}/google-cloud-sdk/path.bash.inc" && \
 source "${HOME}/google-cloud-sdk/path.bash.inc"
# The next line enables shell command completion for gcloud.
test -e "${HOME}/google-cloud-sdk/completion.bash.inc" && \
 source "${HOME}/google-cloud-sdk/completion.bash.inc"

test -e "${HOME}/.iterm2_shell_integration.bash" && \
 source "${HOME}/.iterm2_shell_integration.bash"
