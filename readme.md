# Personal dotfiles

A collection of dotfiles for quickly configuring terminal environment

## Highlights

* .vimrc

## Installing

```bash
./install.sh
```

## VIM:

```vim
:PluginInstall
```

## Homebrew:

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

```bash
brew install erlang elixir curt dart lua tmux redis rabbitmq jq hugo fortune ios-deploy rbenv make lynx cowsay android-platform-tools android-sdk android-ndk gradle gpg-agent node python3
sudo gem install lolcat
```

## vim

```bash
mkdir -p github/vim
cd github/vim
git clone https://github.com/vim/vim.git
cd vim
make distclean
make clean
./configure --enable-rubyinterp --enable-pythoninterp  --enable-luainterp --with-lua-prefix=/usr/local --without-x --with-compiledby="Stephan Bothma <sbothma@protonmail.com>"
make
sudo make install
```
